Philip Belcher's New Personal Site
=============================

My Personal Website. Built using Jekyll in combination with Jade (aka Pug), Gulp, SCSS, BrowserSync

Based on shakyShane's jekyll-gulp-sass-browser-sync:  https://github.com/shakyShane/jekyll-gulp-sass-browser-sync