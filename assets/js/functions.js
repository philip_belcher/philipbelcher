$(function(){
  timeMessage();
  formStyler();
  toggleSkillsDetail();
  smoothScroll(300);
});

function timeMessage() {
  var now = new Date();
  var hours = now.getHours();
  var message;
  if(hours < 12) message = "morning";
  else if (hours < 18) message = "afternoon";
  else message = "evening";

  $('#timeofday').text(message);
}

function formStyler() {
  //cache the elements for easy reference
  //using jQuery's :input selector, auto selects inputs and textareas from the form - handy!
  var form_elements = $('#contact-form :input');
  form_elements.focusout(function(){
    var text_value = $(this).val();
    if(text_value === "") {
      $(this).removeClass('not-empty');
    } else {
      $(this).addClass('not-empty');
    }
  });
}

function smoothScroll (duration) {
	$('a[href^="#"]').on('click', function(event) {

	    var target = $( $(this).attr('href') );

	    if( target.length ) {
	        event.preventDefault();
	        $('html, body').animate({
	            scrollTop: target.offset().top
	        }, duration);
	    }
	});
}

function toggleSkillsDetail() {
  $(".expandyskills").click(function () {
      $expandyskills = $(this);
      $content = $('.skill-details')
      $content.slideToggle(700, function () {
          $expandyskills.text(function () {
              return $content.is(":visible") ? "hide the skills detail" : "view more skills detail";
          });
      });

  });
}
